#/bin/bash

MODEL_DIR=$1

# HERO-flat experiments with randomly generated response retrieval dataset
allennlp train config/goal/hero-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/hero_flat_bs_32_lr_2e-5 --include-package hero --override "{'trainer': {'optimizer': {'lr': 2e-5}}}";
allennlp train config/goal/hero-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/hero_flat_bs_32_lr_5e-5 --include-package hero --override "{'trainer': {'optimizer': {'lr': 5e-5}}}";
allennlp train config/goal/hero-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/hero_flat_bs_32_lr_2e-6 --include-package hero --override "{'trainer': {'optimizer': {'lr': 2e-6}}}";
allennlp train config/goal/hero-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/hero_flat_bs_32_lr_5e-6 --include-package hero --override "{'trainer': {'optimizer': {'lr': 5e-6}}}";
allennlp train config/goal/hero-frame-reordering.jsonnet -s $MODEL_DIR/frame_reordering/hero_flat_bs_32_lr_1e-4 --include-package hero --override "{'trainer': {'optimizer': {'lr': 1e-4}}}";
