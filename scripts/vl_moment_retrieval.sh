#/bin/bash

MODEL_DIR=$1

allennlp train config/goal/vl-moment-retrieval.jsonnet -s $MODEL_DIR/moment_retrieval/vl_bs_64_lr_2e-5 --include-package hero --override "{'trainer': {'optimizer': {'lr': 2e-5}}}";
allennlp train config/goal/vl-moment-retrieval.jsonnet -s $MODEL_DIR/moment_retrieval/vl_bs_64_lr_5e-5 --include-package hero --override "{'trainer': {'optimizer': {'lr': 5e-5}}}";
allennlp train config/goal/vl-moment-retrieval.jsonnet -s $MODEL_DIR/moment_retrieval/vl_bs_64_lr_2e-6 --include-package hero --override "{'trainer': {'optimizer': {'lr': 2e-6}}}";
allennlp train config/goal/vl-moment-retrieval.jsonnet -s $MODEL_DIR/moment_retrieval/vl_bs_64_lr_5e-6 --include-package hero --override "{'trainer': {'optimizer': {'lr': 5e-6}}}";
