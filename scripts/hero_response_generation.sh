#/bin/bash

MODEL_DIR=$1

# HERO-flat experiments with randomly generated response retrieval dataset
allennlp train config/goal/hero-response-generation.jsonnet -s $MODEL_DIR/generation/hero_flat_bs_32_lr_3e-4_tieweights --include-package hero --override "{'trainer': {'optimizer': {'lr': 3e-4}}, 'model': {'tie_encoder_decoder': true}}";
allennlp train config/goal/hero-response-generation.jsonnet -s $MODEL_DIR/generation/hero_flat_bs_32_lr_3e-4_notieweights --include-package hero --override "{'trainer': {'optimizer': {'lr': 3e-4}}, 'model': {'tie_encoder_decoder': false}}";

allennlp train config/goal/hero-response-generation.jsonnet -s $MODEL_DIR/generation/hero_flat_bs_32_lr_1e-4_tieweights --include-package hero --override "{'trainer': {'optimizer': {'lr': 1e-4}}, 'model': {'tie_encoder_decoder': true}}";
allennlp train config/goal/hero-response-generation.jsonnet -s $MODEL_DIR/generation/hero_flat_bs_32_lr_2e-5_tieweights --include-package hero --override "{'trainer': {'optimizer': {'lr': 2e-5}}, 'model': {'tie_encoder_decoder': true}}";
allennlp train config/goal/hero-response-generation.jsonnet -s $MODEL_DIR/generation/hero_flat_bs_32_lr_5e-5_tieweights --include-package hero --override "{'trainer': {'optimizer': {'lr': 5e-5}}, 'model': {'tie_encoder_decoder': true}}";
allennlp train config/goal/hero-response-generation.jsonnet -s $MODEL_DIR/generation/hero_flat_bs_32_lr_1e-4_notieweights --include-package hero --override "{'trainer': {'optimizer': {'lr': 1e-4}}, 'model': {'tie_encoder_decoder': false}}";
allennlp train config/goal/hero-response-generation.jsonnet -s $MODEL_DIR/generation/hero_flat_bs_32_lr_2e-5_notieweights --include-package hero --override "{'trainer': {'optimizer': {'lr': 2e-5}}, 'model': {'tie_encoder_decoder': false}}";
allennlp train config/goal/hero-response-generation.jsonnet -s $MODEL_DIR/generation/hero_flat_bs_32_lr_5e-5_notieweights --include-package hero --override "{'trainer': {'optimizer': {'lr': 5e-5}}, 'model': {'tie_encoder_decoder': false}}";
