#!/usr/bin/env bash

source activate goal

# Common paths
export CURRENT_DIR=${PWD}
export PARENT_DIR="$(dirname "$CURRENT_DIR")"
cd $PARENT_DIR

PYTHONPATH=. python tests/test_goal_gen_model.py