local model_name = "roberta-base";
local model_path = "storage/models/pretrained/hero-tv-ht100.pt";
local do_lowercase = true;
local train_data = "tests/fixtures/tiny.jsonl";
local valid_data = "tests/fixtures/tiny.jsonl";
local batch_size = 3;
local max_history_length = 50;
local max_target_length = 25;

{
    "dataset_reader" : {
        "type": "goal-response-generation",
        "video_db": {
            "path": "storage/football_data/video_db/",
            "feat_version": "resnet_slowfast",
            "frame_interval": 1.5,
            "compress": false
        },
        "txt_db_path": "storage/football_data/finegrained_txt_db",
        "max_history_length": max_history_length,
        "max_target_length": max_target_length,
        "tokenizer": {
            "type": "pretrained_transformer",
            "model_name": model_name,
            "add_special_tokens": false
        },
        "token_indexers": {
            "tokens": {
                "type": "pretrained_transformer",
                "model_name": model_name
            }
        }
    },
    "train_data_path": train_data,
    "validation_data_path": valid_data,
    "model": {
        "type": "hero-goal-generator",
        "model_path": model_path,
        // These are special parameters for the .generate() method in Huggingface
        // More info: https://huggingface.co/transformers/internal/generation_utils.html
        "generation_params": {
            "max_length": max_target_length,
            "top_p": 0.95,
            "do_sample": true,
            "early_stopping":true
        }
    },
    "data_loader": {
        "type": "gather_dataloader",
        "batch_size": batch_size,
        "shuffle": true
    },
    "trainer": {
        "num_epochs": 50,
        "optimizer": {
          "type": "huggingface_adamw",
          "lr": 5e-5,
          "weight_decay": 0.1,
        },
        "batch_callbacks": [
            {
                "type": "log-decoder-outputs"
            }
        ]
    }
}