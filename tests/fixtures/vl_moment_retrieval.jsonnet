local model_name = "roberta-base";
local model_hidden_dim = 768;
local train_data = "tests/fixtures/bug_data.jsonl";
local valid_data = "tests/fixtures/train.jsonl";
local batch_size = 1;
local max_seq_length = 100;
local num_epochs = 10;
local max_video_length = 100;
{
    "dataset_reader" : {
        "type": "goal-moment-retrieval",
        "video_db": {
            "path": "storage/football_data/video_db/",
            "feat_version": "resnet_slowfast",
            "frame_interval": 1.5,
            "compress": false
        },
        "txt_db_path": "storage/football_data/txt_db/",
        "max_txt_length": max_seq_length,
        "max_video_length": max_video_length,
        "tokenizer": {
            "type": "pretrained_transformer",
            "model_name": model_name,
            "add_special_tokens": true
        },
        "token_indexers": {
            "tokens": {
                "type": "pretrained_transformer",
                "model_name": model_name
            }
        }
    },
    "train_data_path": train_data,
    "validation_data_path": valid_data,
    "model": {
        "type": "vl-moment-retrieval",
        "text_field_embedder": {
          "token_embedders": {
            "tokens": {
              "type": "pretrained_transformer",
              "model_name": model_name
            }
          }
        },
        "pooler": {
           "type": "cls_pooler",
           "embedding_dim": model_hidden_dim,
        }
    },
    "data_loader": {
        "type": "pytorch_dataloader",
        "sampler": "random",
        "batch_size": batch_size
    },
    "trainer": {
        "num_epochs": num_epochs,
        "num_gradient_accumulation_steps": 4,
        "validation_metric": "+exact_match",
        "learning_rate_scheduler": {
          "type": "linear_with_warmup",
          "warmup_steps": 662
        },
        "grad_clipping": 1.0,
        "optimizer": {
          "type": "huggingface_adamw",
          "lr": 5e-05,
          "weight_decay": 0.1
        }
    }
}
