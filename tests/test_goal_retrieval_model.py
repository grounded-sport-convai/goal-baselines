from allennlp.common.testing import ModelTestCase

from hero.model.goal import HeroForGoalResponseRetrieval, HeroForGoalFrameReordering, HeroForMomentRetrieval, \
    SequenceEncoderForGoalFrameReordering
from hero.data.goal import GoalResponseRetrievalReader
from hero.data.gather import GatherDataLoader


class TestGOALRetrievalModel(ModelTestCase):
    def setup_method(self):
        super(TestGOALRetrievalModel, self).setup_method()

    def test_retrieval_model(self):
        self.ensure_model_can_train_save_and_load(
            "tests/fixtures/hero_retrieval.jsonnet"
        )

    def test_hero_frame_reordering_model(self):
        self.ensure_model_can_train_save_and_load(
            "tests/fixtures/hero_frame_reordering.jsonnet"
        )

    def test_seqenc_frame_reordering_model(self):
        self.ensure_model_can_train_save_and_load(
            "tests/fixtures/seqenc-frame-reordering.jsonnet"
        )

    def test_moment_retrieval_vl_model(self):
        self.ensure_model_can_train_save_and_load(
            "tests/fixtures/vl_moment_retrieval.jsonnet"
        )

    def test_moment_retrieval_hero_model(self):
        self.ensure_model_can_train_save_and_load(
            "tests/fixtures/hero-moment-retrieval.jsonnet"
        )
