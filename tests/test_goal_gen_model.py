from allennlp.common.testing import ModelTestCase

from hero.model.goal import HeroForGoalResponseGen
from hero.data.goal import GoalRespGenerationReader
from hero.data.gather import GatherDataLoader
from hero.utils.callbacks import LogDecoderOutputsCallback

class TestGOALGenModel(ModelTestCase):
    def setup_method(self):
        super(TestGOALGenModel, self).setup_method()

    def test_retrieval_model(self):
        self.ensure_model_can_train_save_and_load(
            "tests/fixtures/hero_generation.jsonnet"
        )
