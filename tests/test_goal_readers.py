from allennlp.common import Params
from allennlp.common.testing import AllenNlpTestCase
from allennlp.data import DatasetReader
from hero.data.goal import GoalRespGenerationReader
MODEL_NAME = "roberta-base"
MAX_SEQ_LENGTH = 5


class TestGOALReaders(AllenNlpTestCase):
    def setup_method(self):
        super(TestGOALReaders, self).setup_method()

    def test_retrieval_dataset(self):
        reader = DatasetReader.from_params(Params({
            "type": "goal-retrieval",
            "video_db": {
                "path": "storage/football_data/video_db/",
                "feat_version": "resnet_slowfast",
                "frame_interval": 1.5,
                "compress": False
            },
            "num_neg_candidates": 4,
            "txt_db_path": "storage/football_data/txt_db",
            "max_txt_length": MAX_SEQ_LENGTH,
            "tokenizer": {
                "type": "pretrained_transformer",
                "model_name": MODEL_NAME,
                "add_special_tokens": False
            },
            "token_indexers": {
                "tokens": {
                    "type": "pretrained_transformer",
                    "model_name": MODEL_NAME
                }
            }
        }))

        instances = reader.read("tests/fixtures/train.jsonl")

        print(instances[0])

    def test_frame_reordering_dataset(self):
        reader = DatasetReader.from_params(Params({
            "type": "goal-frame-reordering",
            "video_db": {
                "path": "storage/football_data/video_db/",
                "feat_version": "resnet_slowfast",
                "frame_interval": 1.5,
                "compress": False
            },
            "window_size": 4,
            "txt_db_path": "storage/football_data/txt_db",
            "max_txt_length": MAX_SEQ_LENGTH,
            "tokenizer": {
                "type": "pretrained_transformer",
                "model_name": MODEL_NAME,
                "add_special_tokens": False
            },
            "token_indexers": {
                "tokens": {
                    "type": "pretrained_transformer",
                    "model_name": MODEL_NAME
                }
            }
        }))

        instances = reader.read("tests/fixtures/train.jsonl")

        print(instances[0])

    def test_moment_retrieval_dataset(self):
        reader = DatasetReader.from_params(Params({
            "type": "goal-frame-reordering",
            "video_db": {
                "path": "storage/football_data/video_db/",
                "feat_version": "resnet_slowfast",
                "frame_interval": 1.5,
                "compress": False
            },
            "txt_db_path": "storage/football_data/txt_db",
            "max_txt_length": MAX_SEQ_LENGTH,
            "tokenizer": {
                "type": "pretrained_transformer",
                "model_name": MODEL_NAME,
                "add_special_tokens": False
            },
            "token_indexers": {
                "tokens": {
                    "type": "pretrained_transformer",
                    "model_name": MODEL_NAME
                }
            }
        }))

        instances = reader.read("tests/fixtures/train.jsonl")

        print(instances[0])

    def test_generation_dataset(self):
        reader = DatasetReader.from_params(Params({
            "type": "goal-response-generation",
            "video_db": {
                "path": "storage/football_data/video_db/",
                "feat_version": "resnet_slowfast",
                "frame_interval": 1.5,
                "compress": False
            },
            "txt_db_path": "storage/football_data/txt_db",
            "max_history_length": 50,
            "max_target_length": 50,
            "tokenizer": {
                "type": "pretrained_transformer",
                "model_name": MODEL_NAME,
                "add_special_tokens": False
            },
            "token_indexers": {
                "tokens": {
                    "type": "pretrained_transformer",
                    "model_name": MODEL_NAME
                }
            }
        }))

        instances = reader.read("tests/fixtures/train.jsonl")

        for inst in instances:
            print(inst)
