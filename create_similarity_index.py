import json
import os
from argparse import ArgumentParser

import faiss
import jsonlines
import numpy as np
import torch
from faiss import index_factory
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm
from transformers import AutoModel, AutoTokenizer


class SentenceDataset(Dataset):
    def __init__(self, dataset_dir):
        self.sentences = set()
        for split in ("train", "val", "test"):
            dataset_file = os.path.join(dataset_dir, f"{split}.jsonl")
            self.sentences = self.sentences.union(
                [sentence["caption"] for match in jsonlines.open(dataset_file) for sentence in match["chunks"]])

        self.sentences = list(self.sentences)
        self.id2sentence = {}
        self.sentence2id = {}

        for i, sentence in enumerate(self.sentences):
            self.id2sentence[i] = sentence
            self.sentence2id[sentence] = i

    def return_mapping(self):
        return {
            "id2sentence": self.id2sentence,
            "sentence2id": self.sentence2id
        }

    def __len__(self):
        return len(self.sentences)

    def __getitem__(self, item):
        return item, self.sentences[item]


@torch.no_grad()
def main(args):
    tokenizer = AutoTokenizer.from_pretrained(args.model)
    lang_model = AutoModel.from_pretrained(args.model)

    if not os.path.exists(args.index_dir):
        os.makedirs(args.index_dir)

    index_file = os.path.join(args.index_dir, "goal.index")

    if os.path.exists(index_file):
        raise ValueError(f"The index file {index_file} is already available. "
                         f"Delete the files if you want to rebuild them. Skipping it!")

    # We need to create an index that supports cosine similarity
    # https://github.com/facebookresearch/faiss/wiki/MetricType-and-distances#how-can-i-index-vectors-for-cosine-similarity
    index = index_factory(lang_model.config.hidden_size, "IDMap,Flat", faiss.METRIC_INNER_PRODUCT)

    print(f"Reading dataset splits from directory {args.dataset}")
    dataset = SentenceDataset(args.dataset)
    print(f"Creating similarity index for GOAL dataset...")

    loader = DataLoader(dataset, batch_size=args.batch_size, shuffle=False, num_workers=args.num_workers)

    for ids, sentences in tqdm(loader):
        batch = tokenizer.batch_encode_plus(list(sentences), padding=True, truncation=True, return_tensors="pt")

        encodings = lang_model(**batch).last_hidden_state
        sentence_embeddings = np.ascontiguousarray(encodings[:, 0, :].cpu().numpy())

        # normalize the embeddings
        faiss.normalize_L2(sentence_embeddings)
        ids = np.ascontiguousarray(ids.cpu().numpy())
        index.add_with_ids(sentence_embeddings, ids)

    print(f"Writing index to file {index_file}")
    faiss.write_index(index, index_file)

    index_mapping_file = index_file + ".mapping.json"
    print(f"Writing index mapping to file {index_mapping_file}")
    index_mapping = dataset.return_mapping()
    with open(index_mapping_file, "w") as out_file:
        json.dump(index_mapping, out_file)


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument("--model", type=str, default="roberta-base",
                        help="Language model identifier from https://huggingface.co/")
    parser.add_argument("--dataset", type=str, default="storage/football_data/splits/",
                        help="Path to the Football dataset folder")
    parser.add_argument("--index_dir", type=str, default="storage/football_data/sentence_sim/",
                        help="Path to the folder containing the generated similarity indexes")
    parser.add_argument("--batch_size", type=int, default=8)
    parser.add_argument("--num_workers", type=int, default=0)

    args = parser.parse_args()
    main(args)
