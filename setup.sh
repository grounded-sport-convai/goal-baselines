#!/usr/bin/env bash

# Project directory is the main dir
export CURRENT_DIR=${PWD}
export PROJECT_DIR=${CURRENT_DIR}
# We create data and models
export DATA_DIR=${PROJECT_DIR}/storage/football_data/
export MODEL_DIR=${PROJECT_DIR}/models
mkdir -p ${DATA_DIR}
mkdir -p ${MODEL_DIR}
echo "Data directory created at: " "${DATA_DIR}"
echo "Model directory created at: " "${MODEL_DIR}"
# cd to data directory and download all data
cd ${DATA_DIR}

## Downloading dataset now
# Downloading splits train val test
wget https://www.dropbox.com/sh/96or62sf8ifng4t/AAAhcsdnMn-GGpzPYUEKz5M7a?dl=0 -O splits.zip
unzip splits.zip -d splits && rm splits.zip
# Downloading random_retrieval
wget https://www.dropbox.com/sh/s1gcjvodqxu21oj/AACcERDiKt-q7ZDq_PhjTgiua?dl=0 -O random_retrieval.zip
unzip random_retrieval.zip -d random_retrieval && rm random_retrieval.zip
# Downloading retrieval_dataset
wget https://www.dropbox.com/s/bzrowb2p87j75en/retrieval_dataset.zip?dl=0 -O retrieval_dataset.zip
unzip retrieval_dataset.zip && rm retrieval_dataset.zip
# Downloading sentence_sim
wget https://www.dropbox.com/s/dnoxa6ojzej2sgp/sentence_sim.zip?dl=0 -O sentence_sim.zip
unzip sentence_sim.zip && rm sentence_sim.zip
# Downloading txt_db
wget https://www.dropbox.com/s/vhxtmiiom54r6mi/txt_db.zip?dl=0 -O txt_db.zip
unzip txt_db.zip && rm txt_db.zip
# Downloading video_db
wget https://www.dropbox.com/s/xeh2cay3qqo0ipc/video_db.zip?dl=0 -O video_db.zip
unzip video_db.zip && rm video_db.zip
# Downloading video_features
wget https://www.dropbox.com/s/ieg9lwx1ac0fi4c/video_features.zip?dl=0 -O video_features.zip
unzip video_features.zip && rm video_features.zip

# Conda env and setup
conda create -n goal python=3.7 -y
eval "$(conda shell.bash hook)"
conda init
conda activate goal
conda install jupyter -y
pip install torch==1.7.1+cu110 torchvision==0.8.2+cu110 torchaudio===0.7.2 -f https://download.pytorch.org/whl/torch_stable.html
pip install -r requirements.txt
# pip install opencv-python

#conda install -c anaconda cudatoolkit -y
#conda config --add channels conda-forge
#conda install cudatoolkit-dev
#conda install -c conda-forge cudatoolkit-dev -y

#https://stackoverflow.com/questions/56470424/nvcc-missing-when-installing-cudatoolkit

# Installing apex now
# Also see https://github.com/NVIDIA/apex/issues/988
# https://github.com/NVIDIA/apex/issues/988#issuecomment-726343453
# pip install -v --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ./

# See https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=1804&target_type=debnetwork
#https://towardsdatascience.com/managing-cuda-dependencies-with-conda-89c5d817e7e1

#wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-ubuntu1804.pin
#sudo mv cuda-ubuntu1804.pin /etc/apt/preferences.d/cuda-repository-pin-600
#sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
#sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/ /"
#sudo apt-get update
#sudo apt-get -y install cuda


cd /tmp
git clone https://github.com/NVIDIA/apex
cd apex
pip install -v --disable-pip-version-check --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ./
cd ${DATA_DIR}