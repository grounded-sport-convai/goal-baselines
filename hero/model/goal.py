import os
from typing import Any, List
from typing import Dict

import numpy
import torch
import torch.nn.functional as F
from allennlp.data import Vocabulary, Batch, Instance
from allennlp.data.token_indexers import PretrainedTransformerIndexer
from allennlp.models import Model
from allennlp.modules import FeedForward, TextFieldEmbedder, Seq2VecEncoder, Seq2SeqEncoder
from allennlp.nn import util
from allennlp.nn.util import get_text_field_mask, sequence_cross_entropy_with_logits
from allennlp.training.metrics import CategoricalAccuracy, BLEU, ROUGE
from overrides import overrides
from torch.nn import CrossEntropyLoss
from transformers import EncoderDecoderModel, EncoderDecoderConfig, PreTrainedModel

from hero.data.gather import get_gather_index_from_batch
from hero.eval.moment_retrieval import SpanBasedAccuracy
from hero.eval.ranking import SparseGTMetrics
from hero.model.encoder import RobertaModelConfig
from hero.model.model import HeroModel
from hero.utils.misc import get_best_span

_model_cache = {}


def get_hero_model(model_path, config_path, video_feat_dim, max_frame_seq_len):
    global _model_cache
    state_dict = torch.load(model_path)

    transformer = HeroModel.from_pretrained(
        config_path,
        state_dict,
        vfeat_dim=video_feat_dim,
        max_frm_seq_len=max_frame_seq_len
    )

    _model_cache[model_path] = transformer

    return transformer


class CustomHEROModel(Model):
    def forward_on_instances(self, instances: List[Instance]) -> List[Dict[str, numpy.ndarray]]:
        """
        Takes a list of `Instances`, converts that text into arrays using this model's `Vocabulary`,
        passes those arrays through `self.forward()` and `self.make_output_human_readable()` (which
        by default does nothing) and returns the result.  Before returning the result, we convert
        any `torch.Tensors` into numpy arrays and separate the batched output into a list of
        individual dicts per instance. Note that typically this will be faster on a GPU (and
        conditionally, on a CPU) than repeated calls to `forward_on_instance`.

        # Parameters

        instances : `List[Instance]`, required
            The instances to run the model on.

        # Returns

        A list of the models output for each instance.
        """
        batch_size = len(instances)
        with torch.no_grad():
            cuda_device = self._get_prediction_device()
            dataset = Batch(instances)
            dataset.index_instances(self.vocab)
            # call the collate function here
            gather_index = get_gather_index_from_batch(instances, dataset)
            dataset_dict = dataset.as_tensor_dict()
            dataset_dict["f_gather_index"] = gather_index
            model_input = util.move_to_device(dataset_dict, cuda_device)
            outputs = self.make_output_human_readable(self(**model_input))

            instance_separated_output: List[Dict[str, numpy.ndarray]] = [
                {} for _ in dataset.instances
            ]
            for name, output in list(outputs.items()):
                if isinstance(output, torch.Tensor):
                    # NOTE(markn): This is a hack because 0-dim pytorch tensors are not iterable.
                    # This occurs with batch size 1, because we still want to include the loss in that case.
                    if output.dim() == 0:
                        output = output.unsqueeze(0)

                    if output.size(0) != batch_size:
                        self._maybe_warn_for_unseparable_batches(name)
                        continue
                    output = output.detach().cpu().numpy()
                elif len(output) != batch_size:
                    self._maybe_warn_for_unseparable_batches(name)
                    continue
                for instance_output, batch_element in zip(instance_separated_output, output):
                    instance_output[name] = batch_element
            return instance_separated_output


@Model.register("hero-goal-retrieval")
class HeroForGoalResponseRetrieval(CustomHEROModel):
    def __init__(self,
                 model_path: str,
                 vocab: Vocabulary,
                 config_path: str = None,
                 video_feat_dim=4352,
                 max_frame_seq_len=100
                 ):
        Model.__init__(self, vocab)
        config_path = config_path or os.path.join(os.path.dirname(model_path), 'hero_finetune.json')
        self.hero_model = get_hero_model(model_path, config_path, video_feat_dim, max_frame_seq_len)
        self.dropout = torch.nn.Dropout(self.hero_model.config.f_config.hidden_dropout_prob)
        self.classifier = torch.nn.Linear(self.hero_model.config.f_config.hidden_size, 1)
        self.ranking_metrics = SparseGTMetrics()
        self.video_feat_dim = video_feat_dim
        self.max_frame_seq_len = max_frame_seq_len

    def forward(self, metadata, candidates, f_v_feats, f_v_masks, f_attn_masks, f_gather_index,
                c_v_feats, c_attn_masks, target_index=None):
        batch_size, num_candidates, seq_length = candidates["tokens"]["token_ids"].shape
        f_sub_input_ids = candidates["tokens"]["token_ids"].view(-1, seq_length)
        f_attn_masks = f_attn_masks.view(-1, f_attn_masks.shape[-1])
        # define inputs that require to be repeated for every candidate

        batch = {
            "f_sub_input_ids": f_sub_input_ids,
            "f_v_feats": f_v_feats,
            "f_attn_masks": f_attn_masks,
            "f_gather_index": f_gather_index,
            "f_v_masks": f_v_masks,
            'c_v_feats': c_v_feats,
            'c_attn_masks': c_attn_masks
        }

        for input_key in ("f_v_feats", "f_v_masks", "c_v_feats", "c_attn_masks"):
            orig_shape = batch[input_key].shape
            batch[input_key] = batch[input_key].unsqueeze(1).repeat(1, num_candidates,
                                                                    *[1 for _ in range(len(orig_shape[1:]))])
            batch[input_key] = batch[input_key].view(-1, *orig_shape[1:])

        # Shape: (batch_size*num_candidates, num_vis_feats, hidden_size)
        pooled_output = self.hero_model.v_encoder.f_encoder(batch, 'repr')[1]
        pooled_output = self.dropout(pooled_output)
        logits = self.classifier(pooled_output).view(batch_size, num_candidates)
        result = {"logits": logits}

        if target_index is not None:
            loss = F.cross_entropy(logits, target_index, reduction='mean', ignore_index=-1)

            result["loss"] = loss

            self.ranking_metrics(logits, target_index)
        return result

    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        return self.ranking_metrics.get_metric(reset)


@Model.register("hero-moment-retrieval")
class HeroForMomentRetrieval(CustomHEROModel):
    def __init__(self,
                 model_path: str,
                 vocab: Vocabulary,
                 span_predictor: FeedForward,
                 config_path: str = None,
                 video_feat_dim=4352,
                 max_frame_seq_len=100
                 ):
        Model.__init__(self, vocab)
        config_path = config_path or os.path.join(os.path.dirname(model_path), 'hero_finetune.json')
        self.hero_model = get_hero_model(model_path, config_path, video_feat_dim, max_frame_seq_len)
        self.video_feat_dim = video_feat_dim
        self.max_frame_seq_len = max_frame_seq_len
        self.span_accuracy = SpanBasedAccuracy()
        self.span_predictor = span_predictor

    def forward(self, metadata, query, f_v_feats, f_v_masks, f_attn_masks,
                f_gather_index, start_index, end_index):
        f_sub_input_ids = query["tokens"]["token_ids"]
        batch = {
            "metadata": metadata,
            "f_sub_input_ids": f_sub_input_ids,
            "caption": query,
            "start_index": start_index,
            "end_index": end_index,
            "f_v_feats": f_v_feats,
            "f_attn_masks": f_attn_masks,
            "f_v_masks": f_v_masks,
            "f_gather_index": f_gather_index
        }

        output_dict = {}

        embeddings = self.hero_model.v_encoder.f_encoder(batch, 'repr')[0]
        logits = self.span_predictor(embeddings)
        start_logits, end_logits = logits.split(1, dim=-1)
        start_logits = start_logits.squeeze(-1)
        end_logits = end_logits.squeeze(-1)

        if start_index is not None and end_index is not None:
            # If we are on multi-GPU, split add a dimension
            if len(start_index.size()) > 1:
                start_index = start_index.squeeze(-1)
            if len(end_index.size()) > 1:
                end_index = end_index.squeeze(-1)
            # sometimes the start/end positions are outside our model inputs, we ignore these terms
            ignored_index = start_logits.size(1)
            start_index.clamp_(0, ignored_index)
            end_index.clamp_(0, ignored_index)

            loss_fct = CrossEntropyLoss(ignore_index=ignored_index)
            start_loss = loss_fct(start_logits, start_index)
            end_loss = loss_fct(end_logits, end_index)
            total_loss = (start_loss + end_loss) / 2

            output_dict["loss"] = total_loss

            # finds the best span satisfying positional constraints
            best_span_logits = get_best_span(start_logits, end_logits)
            gold_spans = torch.cat([start_index.unsqueeze(-1), end_index.unsqueeze(-1)], -1)

            self.span_accuracy(best_span_logits, gold_spans)

        return output_dict

    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        return self.span_accuracy.get_metric(reset)


@Model.register("seqenc-goal-reordering")
class SequenceEncoderForGoalFrameReordering(CustomHEROModel):
    def __init__(self,
                 seq_encoder: Seq2SeqEncoder,
                 classifier: FeedForward,
                 vocab: Vocabulary,
                 video_feat_dim=4352,
                 max_frame_seq_len=100,
                 repr_dropout=0.1
                 ):
        Model.__init__(self, vocab)
        self.seq_encoder = seq_encoder
        self.classifier = classifier
        self.visual_transform = torch.nn.Sequential(
            torch.nn.Linear(video_feat_dim, self.seq_encoder.get_input_dim()),
            torch.nn.ReLU(),
            torch.nn.Dropout(repr_dropout)
        )
        self.accuracy = CategoricalAccuracy()
        self.video_feat_dim = video_feat_dim
        self.max_frame_seq_len = max_frame_seq_len

    def forward(self, metadata, caption, f_v_feats, f_v_masks, f_attn_masks, f_gather_index,
                target_pos, target_mask, target_orders=None):
        hidden_size = self.seq_encoder.get_output_dim()
        # Shape: (batch_size, num_vis_feats, hidden_size)
        embeddings = self.visual_transform(f_v_feats)
        frame_features = self.seq_encoder(embeddings, f_v_masks.bool())
        target_pos = target_pos.unsqueeze(-1).expand(-1, -1, hidden_size).long()
        frame_features = frame_features.gather(1, target_pos)
        batch_size, num_frames, _ = frame_features.shape
        logits = self.classifier(frame_features.view(-1, hidden_size))
        logits = logits.view(batch_size * num_frames, -1)

        result = {"logits": logits}

        if target_orders is not None:
            target_orders = target_orders.view(batch_size * num_frames)

            # Compute the cross entropy loss
            loss = F.cross_entropy(
                logits,
                target_orders,
                reduction="none"
            ).view(batch_size, num_frames)

            # We mask the loss
            loss = loss * target_mask

            # We make sure to sum over the frames dimension and then we do mean over the batch ignoring the 0 elements
            result["loss"] = loss.sum(-1).mean()

            predictions = torch.softmax(logits, -1).view(batch_size * num_frames, -1)

            self.accuracy(
                predictions,
                target_orders.view(batch_size * num_frames),
                target_mask.view(batch_size * num_frames)
            )

        return result

    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        return {"accuracy": self.accuracy.get_metric(reset)}


@Model.register("hero-goal-reordering")
class HeroForGoalFrameReordering(CustomHEROModel):
    def __init__(self,
                 classifier: FeedForward,
                 model_path: str,
                 vocab: Vocabulary,
                 config_path: str = None,
                 video_feat_dim=4352,
                 max_frame_seq_len=100
                 ):
        Model.__init__(self, vocab)
        config_path = config_path or os.path.join(os.path.dirname(model_path), 'hero_finetune.json')
        self.hero_model = get_hero_model(model_path, config_path, video_feat_dim, max_frame_seq_len)
        self.classifier = classifier
        self.accuracy = CategoricalAccuracy()
        self.video_feat_dim = video_feat_dim
        self.max_frame_seq_len = max_frame_seq_len

    def forward(self, metadata, caption, f_v_feats, f_v_masks, f_attn_masks, f_gather_index,
                target_pos, target_mask, target_orders=None):
        f_sub_input_ids = caption["tokens"]["token_ids"]
        # define inputs that require to be repeated for every candidate

        batch = {
            "f_sub_input_ids": f_sub_input_ids,
            "f_v_feats": f_v_feats,
            "f_attn_masks": f_attn_masks,
            "f_gather_index": f_gather_index,
            "f_v_masks": f_v_masks
        }
        hidden_size = self.hero_model.config.f_config.hidden_size
        target_pos = target_pos.unsqueeze(-1).expand(-1, -1, hidden_size).long()

        # Shape: (batch_size, num_vis_feats, hidden_size)
        embeddings = self.hero_model.v_encoder.f_encoder(batch, 'repr')[0]
        frame_features = embeddings.gather(1, target_pos)

        batch_size, num_frames, _ = frame_features.shape
        logits = self.classifier(frame_features.view(-1, hidden_size))
        logits = logits.view(batch_size * num_frames, -1)

        result = {"logits": logits}

        if target_orders is not None:
            target_orders = target_orders.view(batch_size * num_frames)

            # Compute the cross entropy loss
            loss = F.cross_entropy(
                logits,
                target_orders,
                reduction="none"
            ).view(batch_size, num_frames)

            # We mask the loss
            loss = loss * target_mask

            # We make sure to sum over the frames dimension and then we do mean over the batch ignoring the 0 elements
            result["loss"] = loss.sum(-1).mean()

            predictions = torch.softmax(logits, -1).view(batch_size * num_frames, -1)

            self.accuracy(
                predictions,
                target_orders.view(batch_size * num_frames),
                target_mask.view(batch_size * num_frames)
            )

        return result

    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        return {"accuracy": self.accuracy.get_metric(reset)}


@Model.register("vl-moment-retrieval")
class VLForMomentRetrieval(CustomHEROModel):
    def __init__(self,
                 text_field_embedder: TextFieldEmbedder,
                 pooler: Seq2VecEncoder,
                 vocab: Vocabulary,
                 video_feat_dim=4352,
                 max_frame_seq_len=100,
                 repr_dropout=0.1
                 ):
        Model.__init__(self, vocab)
        self.text_field_embedder = text_field_embedder
        self.pooler = pooler
        self.visual_transform = torch.nn.Sequential(
            torch.nn.Linear(video_feat_dim, self.text_field_embedder.get_output_dim()),
            torch.nn.ReLU(),
            torch.nn.Dropout(repr_dropout)
        )
        self.span_predictor = torch.nn.Linear(self.text_field_embedder.get_output_dim(), 2)
        self.video_feat_dim = video_feat_dim
        self.max_frame_seq_len = max_frame_seq_len
        self.span_accuracy = SpanBasedAccuracy()

    def forward(self, metadata, query, f_v_feats, f_v_masks, f_attn_masks, start_index, end_index):
        token_embeddings = self.text_field_embedder(query)
        f_v_feats_embeddings = self.visual_transform(f_v_feats)
        query_embeddings = self.pooler(token_embeddings)
        query_embeddings = query_embeddings.unsqueeze(1).expand(-1, f_v_feats_embeddings.shape[1], -1)

        features = torch.cat([f_v_feats_embeddings, query_embeddings], 1)
        output_dict = {}
        # Code inspired by BertForQuestionAnswering.forward()
        logits = self.span_predictor(features)
        start_logits, end_logits = logits.split(1, dim=-1)
        start_logits = start_logits.squeeze(-1)
        end_logits = end_logits.squeeze(-1)

        if start_index is not None and end_index is not None:
            # If we are on multi-GPU, split add a dimension
            if len(start_index.size()) > 1:
                start_index = start_index.squeeze(-1)
            if len(end_index.size()) > 1:
                end_index = end_index.squeeze(-1)
            # sometimes the start/end positions are outside our model inputs, we ignore these terms
            ignored_index = start_logits.size(1)
            start_index.clamp_(0, ignored_index)
            end_index.clamp_(0, ignored_index)

            loss_fct = CrossEntropyLoss(ignore_index=ignored_index)
            start_loss = loss_fct(start_logits, start_index)
            end_loss = loss_fct(end_logits, end_index)
            total_loss = (start_loss + end_loss) / 2

            output_dict["loss"] = total_loss

            #  get_best_span
            best_span_logits = get_best_span(start_logits, end_logits)
            gold_spans = torch.cat([start_index.unsqueeze(-1), end_index.unsqueeze(-1)], -1)

            self.span_accuracy(best_span_logits, gold_spans)

        return output_dict

    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        return self.span_accuracy.get_metric(reset)


def _tie_encoder_decoder_weights(_, encoder: torch.nn.Module, decoder: torch.nn.Module, base_model_prefix: str):
    uninitialized_encoder_weights: List[str] = []
    if decoder.__class__ != encoder.__class__:
        print(
            f"{decoder.__class__} and {encoder.__class__} are not equal. In this case make sure that all encoder weights are correctly initialized."
        )

    def tie_encoder_to_decoder_recursively(
            decoder_pointer: torch.nn.Module,
            encoder_pointer: torch.nn.Module,
            module_name: str,
            uninitialized_encoder_weights: List[str],
            depth=0,
    ):
        # This is the base step: we associated the *encoder* weights and bias to the *decoder*
        # not the other way around
        assert isinstance(decoder_pointer, torch.nn.Module) and isinstance(
            encoder_pointer, torch.nn.Module
        ), f"{decoder_pointer} and {encoder_pointer} have to be of type torch.nn.Module"
        if hasattr(decoder_pointer, "weight"):
            assert hasattr(encoder_pointer, "weight")
            decoder_pointer.weight = encoder_pointer.weight
            if hasattr(decoder_pointer, "bias"):
                assert hasattr(encoder_pointer, "bias")
                decoder_pointer.bias = encoder_pointer.bias
            return

        encoder_modules = encoder_pointer._modules
        decoder_modules = decoder_pointer._modules
        if len(decoder_modules) > 0:
            assert (
                    len(encoder_modules) > 0
            ), f"Encoder module {encoder_pointer} does not match decoder module {decoder_pointer}"

            all_encoder_weights = set([module_name + "/" + sub_name for sub_name in encoder_modules.keys()])
            encoder_layer_pos = 0
            for name, module in decoder_modules.items():
                if name.isdigit():
                    encoder_name = str(int(name) + encoder_layer_pos)
                    decoder_name = name
                    if not isinstance(decoder_modules[decoder_name], type(encoder_modules[encoder_name])) and len(
                            encoder_modules
                    ) != len(decoder_modules):
                        # this can happen if the name corresponds to the position in a list module list of layers
                        # in this case the decoder has added a cross-attention that the encoder does not have
                        # thus skip this step and subtract one layer pos from encoder
                        encoder_layer_pos -= 1
                        continue
                elif name not in encoder_modules:
                    continue
                elif depth > 500:
                    raise ValueError(
                        "Max depth of recursive function `tie_encoder_to_decoder` reached. It seems that there is a circular dependency between two or more `nn.Modules` of your model."
                    )
                else:
                    decoder_name = encoder_name = name
                tie_encoder_to_decoder_recursively(
                    decoder_modules[decoder_name],
                    encoder_modules[encoder_name],
                    module_name + "/" + name,
                    uninitialized_encoder_weights,
                    depth=depth + 1,
                )
                all_encoder_weights.remove(module_name + "/" + encoder_name)

            uninitialized_encoder_weights += list(all_encoder_weights)

    # tie weights recursively
    tie_encoder_to_decoder_recursively(decoder, encoder, base_model_prefix, uninitialized_encoder_weights)
    if len(uninitialized_encoder_weights) > 0:
        print(
            f"The following encoder weights were not tied to the decoder {uninitialized_encoder_weights}"
        )


@Model.register("hero-goal-generator")
class HeroForGoalResponseGen(CustomHEROModel):
    BASE_MODEL_NAME = "roberta-base"
    BASE_MODEL_TYPE = "roberta"

    def __init__(self,
                 model_path: str,
                 vocab: Vocabulary,
                 config_path: str = None,
                 video_feat_dim=4352,
                 max_frame_seq_len=100,
                 generation_params=None,
                 tie_encoder_decoder=False,
                 tie_word_embeddings=True
                 ):
        Model.__init__(self, vocab)
        config_path = config_path or os.path.join(os.path.dirname(model_path), 'hero_finetune.json')
        encoder_model = get_hero_model(model_path, config_path, video_feat_dim, max_frame_seq_len)
        self._indexer = PretrainedTransformerIndexer(self.BASE_MODEL_NAME, namespace="tokens")
        encoder_config = encoder_model.config.f_config.to_dict()
        decoder_config = encoder_model.config.f_config.to_dict()
        encoder_config.update({"model_type": self.BASE_MODEL_TYPE})
        decoder_config.update({"model_type": self.BASE_MODEL_TYPE})
        self.config = EncoderDecoderConfig.from_encoder_decoder_configs(
            RobertaModelConfig.from_dict(encoder_config),
            RobertaModelConfig.from_dict(decoder_config)
        )

        self.config.tie_encoder_decoder = tie_encoder_decoder
        self.config.tie_word_embeddings = tie_word_embeddings

        PreTrainedModel._tie_encoder_decoder_weights = _tie_encoder_decoder_weights

        self.enc_dec = EncoderDecoderModel(
            self.config,
            encoder_model.v_encoder.f_encoder
        )

        if not tie_encoder_decoder:
            self.enc_dec.decoder.lm_head = self.enc_dec.encoder.lm_head

        self.video_feat_dim = video_feat_dim
        self.max_frame_seq_len = max_frame_seq_len

        self.config.pad_token_id = self.enc_dec.decoder.config.pad_token_id
        self.config.eos_token_id = self.enc_dec.decoder.config.eos_token_id
        self.config.bos_token_id = self.enc_dec.decoder.config.bos_token_id
        self.generation_params = generation_params
        # # We need to make sure that Huggingface uses these token ids in the generation process
        self.generation_params.update({
            "eos_token_id": self.config.eos_token_id,
            "bos_token_id": self.config.bos_token_id,
            "pad_token_id": self.config.pad_token_id
        })
        for param, val in self.generation_params.items():
            setattr(self.config, param, val)
        self._rouge = ROUGE(
            exclude_indices={self.config.bos_token_id, self.config.pad_token_id, self.config.eos_token_id})
        self._bleu = BLEU(
            exclude_indices={self.config.bos_token_id, self.config.pad_token_id, self.config.eos_token_id})

    def forward(self, metadata, history, f_attn_masks, f_gather_index, f_v_feats, f_v_masks, c_v_feats, c_attn_masks,
                targets=None):
        batch = {
            "f_sub_input_ids": history["tokens"]["token_ids"],
            "f_v_feats": f_v_feats,
            "f_attn_masks": f_attn_masks,
            "f_gather_index": f_gather_index,
            "f_v_masks": f_v_masks
        }

        result = {}

        if self.training:
            decoder_input_ids = targets["tokens"]["token_ids"][:, :-1].contiguous().long()
            decoder_targets = targets["tokens"]["token_ids"][:, 1:].contiguous().long()
            decoder_mask = get_text_field_mask(targets)
            decoder_input_mask = decoder_mask[:, :-1].bool()
            decoder_targets_mask = decoder_mask[:, 1:].bool()
            encoder_attention_mask = f_attn_masks
            # Run the HERO encoder first and get corresponding hidden states
            encoder_outputs = self.enc_dec.encoder(batch, 'repr')
            encoder_hidden_states = encoder_outputs[0]

            # Run Roberta decoder using HERO hidden states and attention masks
            decoder_outputs = self.enc_dec.decoder(
                input_ids=decoder_input_ids,
                encoder_hidden_states=encoder_hidden_states,
                encoder_attention_mask=encoder_attention_mask,
                attention_mask=decoder_input_mask
            )

            # Compute seq2seq loss function ala BART
            result["loss"] = sequence_cross_entropy_with_logits(
                decoder_outputs.logits,
                decoder_targets,
                decoder_targets_mask,
                label_smoothing=0.1,
                average="token",
            )

            result["logits"] = decoder_outputs.logits
        else:
            # trick to include the correct attention mask for the current input
            batch["attention_mask"] = f_attn_masks

            labels = targets["tokens"]["token_ids"]
            predicted_token_ids = self.enc_dec.generate(
                input_ids=history["tokens"]["token_ids"],
                **self.generation_params,
                **batch
            )

            self._rouge(predicted_token_ids, labels)
            self._bleu(predicted_token_ids, labels)
            result["predictions"] = predicted_token_ids

            self.make_output_human_readable(result)

        return result

    @overrides
    def make_output_human_readable(self, output_dict: Dict[str, torch.Tensor]) -> Dict[str, Any]:
        """
        # Parameters
        output_dict : `Dict[str, torch.Tensor]`
            A dictionary containing a batch of predictions with key `predictions`. The tensor should have
            shape `(batch_size, max_sequence_length)`
        # Returns
        `Dict[str, Any]`
            Original `output_dict` with an additional `predicted_tokens` key that maps to a list of lists of
            tokens.
        """
        predictions = output_dict["predictions"]
        predicted_captions = [None] * predictions.shape[0]

        for i in range(predictions.shape[0]):
            predicted_captions[i] = self._indexer._tokenizer.decode(predictions[i], skip_special_tokens=True)

        output_dict["predicted_captions"] = predicted_captions

        return output_dict

    @overrides
    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        metrics: Dict[str, float] = {}
        if not self.training:
            rouge = self._rouge.get_metric(reset=reset)
            metrics["ROUGE-L"] = rouge["ROUGE-L"]
            metrics.update(self._bleu.get_metric(reset=reset))
        return metrics
