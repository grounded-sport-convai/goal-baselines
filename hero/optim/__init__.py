"""
Copyright (c) Microsoft Corporation.
Licensed under the MIT license.

Copied from UNITER
(https://github.com/ChenRocks/UNITER)
"""
from .adamw import AdamW
from .sched import noam_schedule, warmup_linear, vqa_schedule, get_lr_sched
