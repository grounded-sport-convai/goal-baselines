## Evaluation

- For statistical NLG evaluation metrics, we rely on the [nlg-eval](https://github.com/Maluuba/nlg-eval) repo.

```
pip install git+https://github.com/Maluuba/nlg-eval.git@master
nlg-eval --setup
```

Pre-requisite: Check that you have java installed.  Install the latest java
```
# https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-get-on-ubuntu-16-04
sudo apt-get install default-jre
sudo apt-get install default-jdk
```
