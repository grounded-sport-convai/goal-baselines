from typing import Dict, Any, Optional

import torch
from allennlp.training.metrics import Metric


class SpanBasedAccuracy(Metric):
    def __init__(self):
        self.count_exact = 0
        self.count_soft = 0
        self.score_match = 0.0
        self.total_count = 0

    def __call__(self, predictions: torch.Tensor, gold_labels: torch.Tensor, mask: Optional[torch.BoolTensor] = None):
        # we assume that the input tensor is (batch_size, 2) where 2 is the start and end position
        assert predictions.shape == gold_labels.shape, "Predictions shape should match gold labels shape"
        predictions, gold_labels = self.detach_tensors(predictions, gold_labels)

        predictions_list = [range(*p) for p in predictions]
        gold_labels_list = [range(*p) for p in gold_labels]

        for pred, gold in zip(predictions_list, gold_labels_list):
            self.total_count += 1
            if pred == gold:
                self.count_exact += 1
                self.count_soft += 1
                self.score_match += 1.0
            else:
                pred_set = set(pred)
                soft_matches = len(pred_set.intersection(gold))
                union_matches = len(pred_set.union(gold))

                if soft_matches > 0:
                    self.count_soft += 1
                    self.score_match += (soft_matches / union_matches)

    def get_metric(self, reset: bool = False) -> Dict[str, Any]:
        stats = {}

        if self.total_count > 0:
            stats["exact_match"] = self.count_exact / self.total_count
            stats["soft_match"] = self.count_soft / self.total_count
            stats["weighted_match"] = self.score_match / self.total_count
        else:
            stats["exact_match"] = 0.0
            stats["soft_match"] = 0.0
            stats["weighted_match"] = 0.0

        if reset:
            self.reset()

        return stats

    def reset(self) -> None:
        self.count_soft = 0.0
        self.count_exact = 0
        self.score_match = 0
        self.total_count = 0


if __name__ == "__main__":
    predictions = torch.tensor([
        [4, 10],
        [5, 10],
        [10, 11]
    ])

    gold_labels = torch.tensor([
        [4, 8],
        [5, 10],
        [10, 15]
    ])

    accuracy = SpanBasedAccuracy()

    accuracy(predictions, gold_labels)

    print(accuracy.get_metric())
