import copy
import json
import os
import random
from argparse import ArgumentParser

import faiss
import jsonlines
import numpy as np
import torch
from tqdm import tqdm
from transformers import AutoModel, AutoTokenizer

random.seed(12345)
SPLIT_NAMES = ("train", "val", "test")


@torch.no_grad()
def faiss_sampling(args):
    tokenizer = AutoTokenizer.from_pretrained(args.model)
    lang_model = AutoModel.from_pretrained(args.model)

    if not os.path.exists(args.output):
        os.makedirs(args.output)

    print(f"Loading similarity index")
    index_file = os.path.join(args.index_dir, f"goal.index")
    index_file_mapping = os.path.join(args.index_dir, f"goal.index.mapping.json")

    if not os.path.exists(index_file):
        raise ValueError(f"Index file not available: {index_file}")

    with open(index_file_mapping) as in_file:
        index_mapping = json.load(in_file)

    index = faiss.read_index(index_file)

    for split in SPLIT_NAMES:
        dataset_file = os.path.join(args.dataset, f"{split}.jsonl")
        # we create batches based on matches

        with jsonlines.open(dataset_file) as in_file:
            output_file = os.path.join(args.output, f"{split}.jsonl")
            with jsonlines.open(output_file, mode="w") as out_file:
                for match_data in tqdm(in_file, desc="Processing match files"):
                    sentences = [s["caption"] for s in match_data["chunks"]]

                    batch = tokenizer.batch_encode_plus(sentences, padding=True, truncation=True, return_tensors="pt")

                    encodings = lang_model(**batch).last_hidden_state
                    sentence_embeddings = np.ascontiguousarray(encodings[:, 0, :].cpu().numpy())
                    faiss.normalize_L2(sentence_embeddings)
                    _, neg_candidates = index.search(-1 * sentence_embeddings, 2 * (args.num_candidates - 1))

                    new_data = copy.deepcopy(match_data)

                    for i, chunk in enumerate(new_data["chunks"]):
                        curr_negatives = random.sample(neg_candidates[i].tolist(), k=args.num_candidates - 1)
                        new_data["chunks"][i]["negative_candidates"] = [
                            index_mapping["id2sentence"][str(neg_id)] for neg_id in curr_negatives
                        ]

                    out_file.write(new_data)


def same_video_sampling(args):
    if not os.path.exists(args.output):
        os.makedirs(args.output)

    for split in SPLIT_NAMES:
        dataset_file = os.path.join(args.dataset, f"{split}.jsonl")
        sentences_set = set()

        with jsonlines.open(dataset_file) as in_file:
            output_file = os.path.join(args.output, f"{split}.jsonl")
            with jsonlines.open(output_file, mode="w") as out_file:
                for match_data in tqdm(in_file, desc="Processing match files"):

                    new_data = copy.deepcopy(match_data)

                    for i, chunk in enumerate(new_data["chunks"]):
                        sentences_set.add(chunk["caption"])

                        # we first sample from the current video captions, if we don't have enough data
                        # we sample from all the other sentences we accumulated so far
                        neg_cand_indexes = [j for j in range(len(new_data['chunks'])) if j != i]
                        neg_cand_indexes = random.sample(
                            neg_cand_indexes,
                            k=min(args.num_candidates - 1, len(neg_cand_indexes))
                        )

                        neg_candidates = set(new_data['chunks'][j]["caption"] for j in neg_cand_indexes)

                        if len(neg_candidates) < args.num_candidates - 1:
                            # sample from the other sentences
                            for sent in sentences_set:
                                neg_candidates.add(sent)

                                if len(neg_candidates) == args.num_candidates - 1:
                                    break
                        assert len(neg_candidates) == args.num_candidates - 1
                        new_data["chunks"][i]["negative_candidates"] = list(neg_candidates)

                    out_file.write(new_data)


def main(args):
    if args.strategy == "faiss":
        print("Starting FAISS-based generation")
        faiss_sampling(args)
    elif args.strategy == "random":
        print("Starting random-based generation")
        same_video_sampling(args)


if __name__ == "__main__":
    parser = ArgumentParser()
    subparsers = parser.add_subparsers(dest="strategy", help='Different types of negative sampling strategies')

    faiss_parser = subparsers.add_parser("faiss")
    faiss_parser.add_argument("--model", type=str, default="roberta-base",
                              help="Language model identifier from https://huggingface.co/")
    faiss_parser.add_argument("--index_dir", default="storage/football_data/sentence_sim", type=str,
                              help="FAISS index for sentence similarity computation")
    faiss_parser.add_argument("--dataset", default="storage/football_data/splits/", type=str,
                              help="Folder containing the Football dataset original splits")
    faiss_parser.add_argument("--num_candidates", type=str, help="Number of candidates for the response retrieval task",
                              default=5)
    faiss_parser.add_argument("--output", required=True, type=str,
                              help="Folder containing the response retrieval splits")

    random_parser = subparsers.add_parser("random")
    random_parser.add_argument("--dataset", default="storage/football_data/splits/", type=str,
                               help="Folder containing the Football dataset original splits")
    random_parser.add_argument("--num_candidates", type=str,
                               help="Number of candidates for the response retrieval task",
                               default=5)
    random_parser.add_argument("--output", required=True, type=str,
                               help="Folder containing the response retrieval splits")

    args = parser.parse_args()

    main(args)
